(function() {

    function User(firstName, lastName, country, subscription, education, about) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.subscription = subscription;
        this.education = education;
        this.about = about;
    }

    User.prototype = {

        toString : function() {
            return 'Имя: ' + this.firstName + '\n' +
                    'Фамилия: ' + this.lastName + '\n' +
                    'Страна:' + this.country + '\n' +
                    'Подписка: ' + this.subscription + '\n' +
                    'Образование: ' + this.education + '\n' +
                    'О себе: ' + this.about + '\n';
        }

    }

    function passed(element) {
        var $element = $(element).parent('div').next('div');
        $element.removeClass('error-active');
        $element.addClass('passed-active');
    }

    function wrong(element) {
        var $element = $(element).parent('div').next('div');
        $element.removeClass('passed-active');
        $element.addClass('error-active');
    }

    function resetButtonOnClick() {

        $('.error-container').removeClass('error-active');
        $('.error-container').removeClass('passed-active');
        $('input[type=text]').val('');
        $('textarea').val('');
        $('#subscribe-checkbox').attr('checked', false);

    }

    function validateInput(input) {

        if (input.value === '') {
           wrong(input);
           return false;
        }
        passed(input);
        return true;
    }

    function validateAll() {
        
        var result = true;
        $('#firstname-input, #lastname-input, #about-textarea')
            .each(function() {
                result = result && validateInput(this);
            });
        return result;
    }

    function submitButtonOnClick() {
    
        if (validateAll()) {    
            var educationText = 'error';
            $('input[name=education]:checked').each(function() {
                var idVal = $(this).attr('id');
                educationText = $('label[for=' + idVal + ']').text();
            })
            var user1 = new User(
                $('#firstname-input').val(),
                $('#lastname-input').val(),
                $('#country-select option:selected').text(),
                $('#subscribe-checkbox').is(':checked'),
                $.trim(educationText),
                $('#about-textarea').val()
            );


            var loadSpinner = new Spinner(document.body);
            loadSpinner.show();
            setTimeout(function() {
                loadSpinner.hide();
                console.log(user1.toString());
            }, 3000);
        }

    }

    function inputChange() {
        validateInput(this);
    }

    (function() {
        $('#submit-button').on('click', submitButtonOnClick);
        $('#reset-button').on('click', resetButtonOnClick)
        $('#firstname-input, #lastname-input, #about-textarea')
            .on('input', inputChange);        
    })();

})();