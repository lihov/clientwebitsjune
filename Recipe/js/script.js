function CreateUl() {
    var containsList = ['Яйцо куриное', 'Сыр', 'Масло', 'Соль', 'Сахар', 'Приправы', 'Петрушка'];

    $('<ul>')
        .addClass('list-contains')
        .attr('id', 'list-ul')
        .appendTo('#list-container-div');

    $.each(containsList, function(i) {
        $('<li>')
            .text(containsList[i])
            .on('mouseover', function() {
                $(this).css('font-weight', 'bold');    
            })
            .on('mouseout', function() {
                $(this).css('font-weight', 'normal');
            })
            .appendTo('#list-ul');
    });
}

function CreateBanner() {
    var container = document.createElement('div');
    container.className = 'banner-container';
    container.innerHTML = '<div id="banner-content-div" class="banner-content"></div> \
        <div id="close-button-div" class="banner-close-button"></div>';
    
    return container;
}

function SetupBanner() {
    var banner = CreateBanner();
    document.body.appendChild(banner);
    var closeButton = document.getElementById('close-button-div');
    var bannerContent = document.getElementById('banner-content-div');
    
    bannerContent.onclick = function() {
        if (banner.classList.contains('banner-container-fanim')) {
            banner.classList.remove('banner-container-fanim');
            banner.classList.add('banner-container-ranim');
        }
        else if (banner.classList.contains('banner-container-ranim')) {
            banner.classList.remove('banner-container-ranim');
            banner.classList.add('banner-container-fanim');
        }
        else {
            banner.classList.add('banner-container-fanim');
        }   
    }

    closeButton.onclick = function() {
        banner.parentNode.removeChild(banner);
    }
}

window.addEventListener('load', CreateUl, false);
window.addEventListener('load', SetupBanner, false);