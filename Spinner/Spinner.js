function Spinner(owner) {
    this.owner = owner;
    
    var self = this;

    function init() {

        $('<style>')
            .prop('type', 'text/css')
            .prop('id', 'spin-keyframes')
            .html('\
            @keyframes spin {\
                0% { transform: rotate(0deg); }\
                100% { transform: rotate(360deg); }\
            }')
            .appendTo('head');
        
        $('<style>')
            .prop('type', 'text/css')
            .prop('id', 'spinner-style')
            .html('\
            .spinner {\
                position: fixed;\
                left: 0px;\
                top: 0px;\
                width: 100%;\
                height: 100%;\
                background-color: black;\
                opacity: 0.5;\
            }')
            .appendTo('head');

        $('<style>')
            .prop('type', 'text/css')
            .prop('id', 'loader-style')
            .html('\
            .loader {\
                position: fixed;\
                left: 50%;\
                top: 30%;\
                border: 8px solid gold;\
                border-top: 8px solid blue;\
                border-bottom: 8px solid blue;\
                border-radius: 50%;\
                width: 30px;\
                height: 30px;\
                opacity: 1.0;\
                animation: spin 2s linear infinite;\
            }')
            .appendTo('head');

        setElements();

    }
    
    function setElements() {

        $('<div>')
            .prop('id', 'background-div')
            .addClass('spinner')
            .appendTo(self.owner);
        
        $('<div>')
            .prop('id', 'loader-div')
            .addClass('loader')
            .appendTo(self.owner);

    }

    function dispose() {

        $('#spin-keyframes').remove();
        $('#spinner-style').remove();
        $('#background-div').remove();
        $('#loader-div').remove();
    }

    this.show = function() {
        init();
    }

    this.hide = function() {
        dispose();
    }

}