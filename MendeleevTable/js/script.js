function IsRequierdCell(cell) {
    return cell.cellIndex % 2 === cell.parentNode.rowIndex % 2;
}

function TableSetup() {
    $('tbody').on('mouseover', 'td', function() {
        if (IsRequierdCell(this)) {
            $(this).addClass('selected-cell');
        }
    });
    $('tbody').on('mouseout', 'td', function() {
        if (IsRequierdCell(this)) {
            $(this).removeClass('selected-cell');
        }
    });
    
    $('tbody').on('click', 'td', function() {
        var clickedCell = $('tbody .clicked-cell');
        if (clickedCell.length === 0) {
            $(this).addClass('clicked-cell');
        }
        else {
            clickedCell.removeClass('clicked-cell');
            var tempHtml = clickedCell.html();
            clickedCell.html($(this).html());
            $(this).html(tempHtml);
        }
    });
}

TableSetup();